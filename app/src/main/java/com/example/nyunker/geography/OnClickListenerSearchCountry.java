package com.example.nyunker.geography;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class OnClickListenerSearchCountry implements View.OnClickListener {
    @Override
    public void onClick(View view) {
        final Context context = view.getContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View formElementsView = inflater.inflate(R.layout.activity_main, null, false);
        final View countryTile = inflater.inflate(R.layout.country_tile, null, false);
        final EditText editTextSearchCountry = (EditText) formElementsView.findViewById(R.id.inputSearchCountry);
        new AlertDialog.Builder(context)
                .setView(countryTile)
                .setTitle("Country Information")
                .setPositiveButton("Close",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }

                        }).show();
    }
}