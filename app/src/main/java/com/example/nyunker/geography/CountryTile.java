package com.example.nyunker.geography;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by nyunker on 1/14/2016.
 */
public class CountryTile extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.country_tile);

        Intent activityThatCalled = getIntent();
        String previousActivity = activityThatCalled.getExtras().getString("callingActivity");
        String countryNameFromPreviousActivity = activityThatCalled.getExtras().getString("countryName");
        String countryLanguageFromPreviousActivity = activityThatCalled.getExtras().getString("countryLanguage");
        String countryRegionFromPreviousActivity = activityThatCalled.getExtras().getString("countryRegion");
        String countryPopulationFromPreviousActivity = activityThatCalled.getExtras().getString("countryPopulation");
        String countryInfoBodyFromPreviousActivity = activityThatCalled.getExtras().getString("countryInfoBody");

        TextView textViewCountryName = (TextView) findViewById(R.id.countryTileName);
        TextView textViewCountryLanguage = (TextView) findViewById(R.id.countryTileLanguage);
        TextView textViewCountryRegion = (TextView) findViewById(R.id.countryTileRegion);
        TextView textViewCountryPopulation = (TextView) findViewById(R.id.countryTilePopulation);
        TextView textViewCountryInfoBody = (TextView) findViewById(R.id.countryTileInfoBody);

        textViewCountryName.append("" + countryNameFromPreviousActivity);
        textViewCountryLanguage.append("" + countryLanguageFromPreviousActivity);
        textViewCountryRegion.append("" + countryRegionFromPreviousActivity);
        textViewCountryPopulation.append("" + countryPopulationFromPreviousActivity);
        textViewCountryInfoBody.append("" + countryInfoBodyFromPreviousActivity);
    }
}
