package com.example.nyunker.geography;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    public TextView getCountryName() {
        return countryName;
    }

    public void setCountryName(TextView countryName) {
        this.countryName = countryName;
    }

    private TextView countryName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button buttonCreateLocation = (Button) findViewById(R.id.buttonCreateCountry);
        buttonCreateLocation.setOnClickListener(new OnClickListenerCreateCountry());
        countryName = (TextView) findViewById(R.id.inputSearchCountry);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        countRecords();
        readRecords();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void countRecords() {
        int recordCount = new TableControllerCountry(this).count();
        TextView textViewRecordCount = (TextView) findViewById(R.id.textViewRecordCount);
        textViewRecordCount.setText(recordCount + " records found.");
    }

    public void readRecords() {

        LinearLayout linearLayoutRecords = (LinearLayout) findViewById(R.id.linearLayoutRecords);
        linearLayoutRecords.removeAllViews();

        List<ObjectCountry> countries = new TableControllerCountry(this).read();

        if (countries.size() > 0) {

            for (ObjectCountry obj : countries) {
                int id = obj.getId();
                String name = obj.getName();
                String language = obj.getLanguage();

                String textViewContents = name + " - " + language;

                TextView textViewCountryItem = new TextView(this);
                textViewCountryItem.setPadding(0, 10, 0, 10);
                textViewCountryItem.setText(textViewContents);
                textViewCountryItem.setTag(Integer.toString(id));
                textViewCountryItem.setOnLongClickListener(new OnLongClickListenerCountryRecord());

                linearLayoutRecords.addView(textViewCountryItem);
            }

        } else {

            TextView locationItem = new TextView(this);
            locationItem.setPadding(8, 8, 8, 8);
            locationItem.setText("No records yet.");

            linearLayoutRecords.addView(locationItem);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickSearchCountry(View view) {
        final TableControllerCountry tableControllerCountry = new TableControllerCountry(this);
        ObjectCountry searchCountry = tableControllerCountry.getCountry(countryName.getText().toString());
        Intent getCountryTileIntent = new Intent(this, CountryTile.class);
        getCountryTileIntent.putExtra("callingActivity", "MainActivity");
        getCountryTileIntent.putExtra("countryName", countryName.getText().toString());
        getCountryTileIntent.putExtra("countryLanguage", searchCountry.getLanguage());
        getCountryTileIntent.putExtra("countryRegion", searchCountry.getRegion());
        getCountryTileIntent.putExtra("countryPopulation", searchCountry.getPopulation());
        getCountryTileIntent.putExtra("countryInfoBody", searchCountry.getInfoBody());
        startActivity(getCountryTileIntent);
    }
}
