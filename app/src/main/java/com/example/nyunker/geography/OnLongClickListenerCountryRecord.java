package com.example.nyunker.geography;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by nyunker on 1/10/2016.
 */
public class OnLongClickListenerCountryRecord implements View.OnLongClickListener {
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private Context context;
    private String id;

    @Override
    public boolean onLongClick(View view) {

        context = view.getContext();
        id = view.getTag().toString();

        final CharSequence[] items = {"View", "Edit", "Delete" };

        new AlertDialog.Builder(context).setTitle("Country Record")
                .setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {

                        if (item == 0) {
                            inspectRecord(Integer.parseInt(id));
                        }

                        else if (item == 1) {

                            editRecord(Integer.parseInt(id));

                        } else if (item == 2) {

                            boolean deleteSuccessful = new TableControllerCountry(context).delete(id);

                            if (deleteSuccessful){
                                Toast.makeText(context, "Country record was deleted.", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(context, "Unable to delete country record.", Toast.LENGTH_SHORT).show();
                            }

                            ((MainActivity) context).countRecords();
                            ((MainActivity) context).readRecords();
                        }
                        dialog.dismiss();

                    }
                }).show();
        return false;
    }

    public void editRecord(final int countryId) {
        final TableControllerCountry tableControllerCountry = new TableControllerCountry(context);
        ObjectCountry objectCountry = tableControllerCountry.readSingleRecord(countryId);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View formElementsView = inflater.inflate(R.layout.country_input_form, null, false);

        final EditText editTextCountryName = (EditText) formElementsView.findViewById(R.id.editTextCountryName);
        final EditText editTextCountryLanguage = (EditText) formElementsView.findViewById(R.id.editTextCountryLanguage);
        final EditText editTextCountryRegion = (EditText) formElementsView.findViewById(R.id.editTextCountryRegion);
        final EditText editTextCountryInfoBody = (EditText) formElementsView.findViewById(R.id.editTextCountryInfoBody);
        final EditText editTextCountryPopulation = (EditText) formElementsView.findViewById(R.id.editTextCountryPopulation);

        editTextCountryName.setText(objectCountry.getName());
        editTextCountryLanguage.setText(objectCountry.getLanguage());
        editTextCountryRegion.setText(objectCountry.getRegion());
        editTextCountryInfoBody.setText(objectCountry.getInfoBody());
        editTextCountryPopulation.setText(objectCountry.getPopulation());

        new AlertDialog.Builder(context)
                .setView(formElementsView)
                .setTitle("Edit Record")
                .setPositiveButton("Save Changes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                ObjectCountry objectCountry = new ObjectCountry();
                                objectCountry.setId(countryId);
                                objectCountry.setName(editTextCountryName.getText().toString());
                                objectCountry.setLanguage(editTextCountryLanguage.getText().toString());
                                objectCountry.setRegion(editTextCountryRegion.getText().toString());
                                objectCountry.setPopulation(editTextCountryPopulation.getText().toString());
                                objectCountry.setInfoBody(editTextCountryInfoBody.getText().toString());

                                boolean updateSuccessful = tableControllerCountry.update(objectCountry);

                                if(updateSuccessful){
                                    Toast.makeText(context, "Country record was updated.", Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(context, "Unable to update Country record.", Toast.LENGTH_SHORT).show();
                                }

                                ((MainActivity) context).countRecords();
                                ((MainActivity) context).readRecords();
                                dialog.cancel();
                            }

                        }).show();
    }

    public void inspectRecord(final int countryId) {
        final TableControllerCountry tableControllerCountry = new TableControllerCountry(context);
        String countryName = tableControllerCountry.readSingleRecord(countryId).getName();

        ObjectCountry searchCountry = tableControllerCountry.getCountry(countryName);
        Intent getCountryTileIntent = new Intent(context, CountryTile.class);
        getCountryTileIntent.putExtra("callingActivity", "MainActivity");
        getCountryTileIntent.putExtra("countryName", countryName);
        getCountryTileIntent.putExtra("countryLanguage", searchCountry.getLanguage());
        getCountryTileIntent.putExtra("countryRegion", searchCountry.getRegion());
        getCountryTileIntent.putExtra("countryPopulation", searchCountry.getPopulation());
        getCountryTileIntent.putExtra("countryInfoBody", searchCountry.getInfoBody());
        context.startActivity(getCountryTileIntent);
    }
}
