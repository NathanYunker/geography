package com.example.nyunker.geography;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 5;
    protected static final String DATABASE_NAME = "CountryDatabase";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE countries " +
                "( id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT, " +
                "language TEXT," +
                "region TEXT, " +
                "population TEXT, " +
                "infoBody TEXT ) ";

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String upgradeQuery = "ALTER TABLE countries ADD COLUMN ";
        if (oldVersion == 5 && newVersion == 6) {
            db.execSQL(upgradeQuery);
        }
//
//        String sql = "DROP TABLE IF EXISTS countries";
//        db.execSQL(sql);
//
//        onCreate(db);
    }

}