package com.example.nyunker.geography;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class TableControllerCountry extends DatabaseHandler {

    public TableControllerCountry(Context context) {
        super(context);
    }

    public boolean create(ObjectCountry objectCountry) {

        ContentValues values = new ContentValues();

        values.put("name", objectCountry.getName());
        values.put("language", objectCountry.getLanguage());
        values.put("region", objectCountry.getRegion());
        values.put("population", objectCountry.getPopulation());
        values.put("infoBody", objectCountry.getInfoBody());

        SQLiteDatabase db = this.getWritableDatabase();

        boolean createSuccessful = db.insert("countries", null, values) > 0;
        db.close();

        return createSuccessful;
    }

    public int count() {

        SQLiteDatabase db = this.getWritableDatabase();

        String sql = "SELECT * FROM countries";
        int recordCount = db.rawQuery(sql, null).getCount();
        db.close();

        return recordCount;

    }

    public ObjectCountry readSingleRecord(int countryId) {

        ObjectCountry objectCountry = null;

        String sql = "SELECT * FROM countries WHERE id = " + countryId;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.moveToFirst()) {

            int id = Integer.parseInt(cursor.getString(cursor.getColumnIndex("id")));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String language = cursor.getString(cursor.getColumnIndex("language"));
            String region = cursor.getString(cursor.getColumnIndex("region"));
            String population = cursor.getString(cursor.getColumnIndex("population"));
            String infoBody = cursor.getString(cursor.getColumnIndex("infoBody"));

            objectCountry = new ObjectCountry();
            objectCountry.setId(id);
            objectCountry.setName(name);
            objectCountry.setLanguage(language);
            objectCountry.setRegion(region);
            objectCountry.setPopulation(population);
            objectCountry.setInfoBody(infoBody);

        }

        cursor.close();
        db.close();

        return objectCountry;

    }

    public boolean update(ObjectCountry objectCountry) {

        ContentValues values = new ContentValues();

        values.put("name", objectCountry.getName());
        values.put("language", objectCountry.getLanguage());
        values.put("region", objectCountry.getRegion());
        values.put("population", objectCountry.getPopulation());
        values.put("infoBody", objectCountry.getInfoBody());

        String where = "id = ?";

        String[] whereArgs = { Integer.toString(objectCountry.getId()) };

        SQLiteDatabase db = this.getWritableDatabase();

        boolean updateSuccessful = db.update("countries", values, where, whereArgs) > 0;
        db.close();

        return updateSuccessful;

    }

    public List<ObjectCountry> read() {

        List<ObjectCountry> recordsList = new ArrayList<ObjectCountry>();

        String sql = "SELECT * FROM countries ORDER BY id DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {

                int id = Integer.parseInt(cursor.getString(cursor.getColumnIndex("id")));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String language = cursor.getString(cursor.getColumnIndex("language"));
                String region = cursor.getString(cursor.getColumnIndex("region"));
                String population = cursor.getString(cursor.getColumnIndex("population"));
                String infoBody = cursor.getString(cursor.getColumnIndex("infoBody"));

                ObjectCountry objectCountry = new ObjectCountry();
                objectCountry.setId(id);
                objectCountry.setName(name);
                objectCountry.setLanguage(language);
                objectCountry.setRegion(region);
                objectCountry.setPopulation(population);
                objectCountry.setInfoBody(infoBody);

                recordsList.add(objectCountry);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return recordsList;
    }

    public boolean delete(String id) {
        boolean deleteSuccessful = false;

        SQLiteDatabase db = this.getWritableDatabase();
        deleteSuccessful = db.delete("countries", "id ='" + id + "'", null) > 0;
        db.close();

        return deleteSuccessful;
    }

    public ObjectCountry getCountry(String name) {

        //TODO: MAJOR REFACTOR OF THIS FUNCTION!!
        Cursor nameCursor = null;
        Cursor languageCursor = null;
        Cursor regionCursor = null;
        Cursor populationCursor = null;
        Cursor infoBodyCursor = null;
        ObjectCountry objectCountry = new ObjectCountry();
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            nameCursor = db.rawQuery("SELECT name FROM countries WHERE name=?", new String[] {name + ""});
            languageCursor = db.rawQuery("SELECT language FROM countries WHERE name=?", new String[] {name + ""});
            regionCursor = db.rawQuery("SELECT region FROM countries WHERE name=?", new String[] {name + ""});
            populationCursor = db.rawQuery("SELECT population FROM countries WHERE name=?", new String[] {name + ""});
            infoBodyCursor = db.rawQuery("SELECT infoBody FROM countries WHERE name=?", new String[] {name + ""});

            if(nameCursor.getCount() > 0) {
                nameCursor.moveToFirst();
                objectCountry.setName(nameCursor.getString(nameCursor.getColumnIndex("name")));
            }
            if(languageCursor.getCount() > 0) {
                languageCursor.moveToFirst();
                objectCountry.setLanguage(languageCursor.getString(languageCursor.getColumnIndex("language")));
            }
            if(regionCursor.getCount() > 0) {
                regionCursor.moveToFirst();
                objectCountry.setRegion(regionCursor.getString(regionCursor.getColumnIndex("region")));
            }
            if(populationCursor.getCount() > 0) {
                populationCursor.moveToFirst();
                objectCountry.setPopulation(populationCursor.getString(populationCursor.getColumnIndex("population")));
            }
            if(infoBodyCursor.getCount() > 0) {
                infoBodyCursor.moveToFirst();
                objectCountry.setInfoBody(infoBodyCursor.getString(infoBodyCursor.getColumnIndex("infoBody")));
            }
            return objectCountry;
        }finally {
        }
    }

    }