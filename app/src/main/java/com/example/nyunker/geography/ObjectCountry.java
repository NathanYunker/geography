package com.example.nyunker.geography;

/**
 * Created by nyunker on 1/10/2016.
 */
public class ObjectCountry {
//make these private!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    private int id;
    private String name;
    private String language;
    private String region;
    private String population;
    private String infoBody;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getInfoBody() {
        return infoBody;
    }

    public void setInfoBody(String infoBody) {
        this.infoBody = infoBody;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public ObjectCountry(){

    }
}
