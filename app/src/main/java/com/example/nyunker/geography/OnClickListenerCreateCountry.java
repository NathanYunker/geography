package com.example.nyunker.geography;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class OnClickListenerCreateCountry implements View.OnClickListener {
    @Override
    public void onClick(View view) {
        final Context context = view.getContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View formElementsView = inflater.inflate(R.layout.country_input_form, null, false);
        final EditText editTextCountryName = (EditText) formElementsView.findViewById(R.id.editTextCountryName);
        final EditText editTextCountryLanguage = (EditText) formElementsView.findViewById(R.id.editTextCountryLanguage);
        final EditText editTextCountryRegion = (EditText) formElementsView.findViewById(R.id.editTextCountryRegion);
        final EditText editTextCountryInfoBody = (EditText) formElementsView.findViewById(R.id.editTextCountryInfoBody);
        final EditText editTextCountryPopulation = (EditText) formElementsView.findViewById(R.id.editTextCountryPopulation);
        new AlertDialog.Builder(context)
                .setView(formElementsView)
                .setTitle("Create Country")
                .setPositiveButton("Add",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String countryName = editTextCountryName.getText().toString();
                                String countryLanguage = editTextCountryLanguage.getText().toString();
                                String countryRegion = editTextCountryRegion.getText().toString();
                                String countryInfoBody = editTextCountryInfoBody.getText().toString();
                                String countryPopulation = editTextCountryPopulation.getText().toString();
                                ObjectCountry objectCountry = new ObjectCountry();
                                objectCountry.setName(countryName);
                                objectCountry.setLanguage(countryLanguage);
                                objectCountry.setRegion(countryRegion);
                                objectCountry.setPopulation(countryPopulation);
                                objectCountry.setInfoBody(countryInfoBody);
                                boolean createSuccessful = new TableControllerCountry(context).create(objectCountry);
                                if(createSuccessful){
                                    Toast.makeText(context, "Country information was saved.", Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(context, "Unable to save Country information.", Toast.LENGTH_SHORT).show();
                                }
                                ((MainActivity) context).countRecords();
                                ((MainActivity) context).readRecords();
                                dialog.cancel();
                            }

                        }).show();

    }
}